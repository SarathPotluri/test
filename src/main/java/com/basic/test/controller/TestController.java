package com.basic.test.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.basic.test.model.Place;
import com.basic.test.repositories.PlaceRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class TestController {
	
    @Autowired
	PlaceRepository pr;
	@RequestMapping("/api/hi")
	public String hi() {
		return "Hello World from Restful API";
	}
	 @RequestMapping(value="/createPlace", method = RequestMethod.POST)

		public Place createPlace(@Valid @RequestBody Place pd)
		{
	    	 pd.setPlaceId(UUID.randomUUID());
	    	System.out.println("Place Data Inserted Successfully");
			return pr.save(pd);
		}
}
