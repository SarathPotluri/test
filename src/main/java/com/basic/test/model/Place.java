package com.basic.test.model;

import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "Place")
public class Place {
	@Id
    private UUID placeId;
	private String place;
	private String state;
	private String country;
	private String pincode;
	
	public Place(UUID placeId, String place, String state, String country, String pincode) {
		super();
		this.placeId = placeId;
		this.place = place;
		this.state = state;
		this.country = country;
		this.pincode = pincode;
	}
	public UUID getPlaceId() {
		return placeId;
	}
	public void setPlaceId(UUID placeId) {
		this.placeId = placeId;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	@Override
	public String toString() {
		return "Place [placeId=" + placeId + ", place=" + place + ", state=" + state + ", country=" + country
				+ ", pincode=" + pincode + "]";
	}

	

}
