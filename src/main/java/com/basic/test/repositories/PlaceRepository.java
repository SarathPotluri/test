package com.basic.test.repositories;



import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.basic.test.model.Place;



@Repository("pr")
public interface PlaceRepository extends MongoRepository<Place, UUID>
{
	Place findByPlaceId(UUID placeId);
}
